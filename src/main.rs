use rand::{thread_rng, Rng};
use crossterm::terminal;
use termion::{clear, cursor, color, terminal_size};
use termion::event::Key;
use termion::input::TermRead;
use std::io::{stdin, stdout, Write};

fn get_grid_pos(key: char, pos: (i16, i16)) -> (i16, i16) {
	let grid_pos: (i16, i16) = match key {
		'w' => (pos.0 - 1, pos.1 - 2),
		'a' => (pos.0 - 2, pos.1 - 1),
		's' => (pos.0 - 1, pos.1),
		'd' => (pos.0, pos.1 - 1),
		_ => (pos.0 - 1, pos.1 - 1),
	};
	(grid_pos.0 as i16, grid_pos.1 as i16)
}

fn check_pos(key: char, mut pos: (i16, i16), grid: (u16, u16), slot: &Vec<Vec<usize>>) -> bool {
	match key {
		'w' => pos.1 = pos.1 - 1,
		'a' => pos.0 = pos.0 - 1,
		's' => pos.1 = pos.1 + 1,
		'd' => pos.0 = pos.0 + 1,
		'q' => pos.1 = 0,
		_ => (),
	}
	if pos.0 <= (grid.0 as i16)
		&& pos.1 <= (grid.1 as i16)
			&& pos.0 >= 1 && pos.1 >= 1 {
		if slot[pos.0 as usize - 1][pos.1 as usize - 1] != 0 {
			true
		} else {
			false
		}
	} else {
		false
	}
}

fn move_cursor(key: char, mut pos: (i16, i16)) -> (i16, i16) {
	match key {
		'w' => pos.1 = pos.1 - 1,
		'a' => pos.0 = pos.0 - 1,
		's' => pos.1 = pos.1 + 1,
		'd' => pos.0 = pos.0 + 1,
		'q' => pos.1 = 0,
		_ => (),
	}
	print!("{}{}+", cursor::Goto(pos.0 as u16, pos.1 as u16), color::Fg(color::Red));
	stdout().flush().unwrap();
	pos
}

fn main() {
	let mut score = 0;
	let mut grid_pos: (i16, i16);

	// tsize.0 = width, tsize.1 = height
	let tsize: (u16, u16) = terminal_size().unwrap();

	// grid.0 = width, grid.1 = height
	let grid: (u16, u16) = (tsize.0, tsize.1 / 2);
	
	// exit height
	let exit_height: u16 = grid.1 as u16 + 3;
	
	// cursor start position
	let mut pos: (i16, i16) = {
		if (grid.0 % 2) == 0 && (grid.1 % 2) == 0 {
			(grid.0 as i16 / 2, grid.1 as i16 / 2)
		} else if (grid.0 % 2) != 0 && (grid.1 % 2) == 0 {
			((grid.0 + 1) as i16 / 2, grid.1 as i16 / 2)
		} else if (grid.0 % 2) == 0 && (grid.1 % 2) != 0 {
			(grid.0 as i16 / 2, (grid.1 + 1) as i16 / 2)
		} else {
			((grid.0 + 1) as i16 / 2, (grid.1 + 1) as i16 / 2)
		}
	};

	// clear screen
	print!("{}{}{}", clear::All, cursor::Goto(1, 1), cursor::Hide);

	// set & print grid slots
	let mut slot = vec![vec![0; grid.1 as usize]; grid.0 as usize];
	for y in 0..grid.1 {
		for x in 0..grid.0 {
			slot[x as usize][y as usize] = thread_rng().gen_range(1..=9);
			print!("{}", slot[x as usize][y as usize]);
			stdout().flush().unwrap();
		}
		println!("");
	}

	// get input chars
	match terminal::enable_raw_mode() {
		Ok(_r) => {
			'amogus: for key in stdin().keys() {
				let key = key.unwrap();
				let key = match key {
					Key::Char(key) => key,
					_ => 'q',
				};
				grid_pos = get_grid_pos(key, pos);
				if check_pos(key, pos, grid, &slot) {
					for _step in 0..slot[grid_pos.0 as usize][grid_pos.1 as usize] {
						if check_pos(key, pos, grid, &slot) {
							grid_pos = get_grid_pos(key, pos);
							slot[grid_pos.0 as usize][grid_pos.1 as usize] = 0;
							pos = move_cursor(key, pos);
							score += 1;
						} else {
							break 'amogus;
						}
					}
				} else {
					break 'amogus;
				}
			}
			match terminal::disable_raw_mode() {
				Ok(_r) => {
					print!("{}{}{}", color::Fg(color::Reset),
						cursor::Goto(1, exit_height), cursor::Show);
					println!("\nscore: {}", score);
				}
				Err(e) => println!("{:?}", e),
			}
		}
		Err(e) => println!("{:?}", e),
	}
}
